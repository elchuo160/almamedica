<!DOCTYPE html>
<html>
<head>
	<!-- Jquery para habilitar funcionalidades de Boostrap -->
	<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<!-- estilo css global -->
	<link rel="stylesheet" type="text/css" href="../css/global.css">

	<!-- script js para funciones asincronas-->
	<script type="text/javascript" src="../js/ajax.js"></script>

	<!-- MDBootstrap Datatables  -->
	<link href="../js/MDB/css/addons/datatables.min.css" rel="stylesheet">

	<!-- MDBootstrap Datatables  -->
	<script type="text/javascript" src="../js/MDB/js/addons/datatables.min.js"></script>

	<title>AlmaMedica</title>
</head>
<body onload="cargarLista();">

	<div class="container">
		<div id="logo"></div>		
		<ul class="nav nav-tabs">
			<li class="nav-item">
				<a class="nav-link active" href="../index.php">Crear Perfil</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="lista.php">Lista de Perfiles</a>
			</li>
		</ul>
		
		<div class="card">
			<div class="card-body">
				<h1 class="card-title">AlmaMedica</h1>
				<h5 class="card-title">Lista de pacientes ingresados: </h5>
				<div id="listau"></div>
			</div>
		</div>
	</div>
</body>
</html>