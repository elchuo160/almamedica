<?php
include('../conexion/conexion.php');

//nos conectamos a la base de datos y obtenemos la iformacion de los usuarios registrados

$sql = 'SELECT * FROM pacientes WHERE 1 ORDER BY apellido';
$r = mysqli_query($conn, $sql);
$rows = mysqli_num_rows($r);
//$f = mysqli_fetch_array($r);
if (mysqli_query($conn, $sql)) {
      echo '<div class="table-responsive">
      			<table id="listau_tab" class="table table-striped">
					<thead class="thead-dark">
					    <tr>
					      <th scope="col">#ID</th>
					      <th scope="col">Foto</th>
					      <th scope="col">Nombre</th>
					      <th scope="col">Apellido</th>
					      <th scope="col">Cedula</th>
					      <th scope="col">Informacion Adicional</th>
					    </tr>
					  </thead>
					<tbody>';
					while ($f = $r->fetch_assoc()) {
        				echo 
						'<tr>
							<th scope="row">'.$f["id"].'</th>
							<th scope="row"><img src="../img/photou/'.$f["photou"].'" class="thumbnail"></th>
							<th scope="row">'.$f["nombre"].'</th>
							<th scope="row">'.$f["apellido"].'</th>
							<th scope="row">'.$f["cedula"].'</th>
							<th scope="row">'.$f["addinfo"].'</th>
						</tr>';
    				}
		echo '  	</tbody>
				</table>
			</div>';
} else {
      echo $conn->error;
}

mysqli_close($conn);