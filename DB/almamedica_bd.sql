-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 15-04-2020 a las 18:36:13
-- Versión del servidor: 5.7.11
-- Versión de PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `almamedica_bd`
--
	CREATE DATABASE almamedica_bd;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pacientes`
--

CREATE TABLE `pacientes` (
  `id` bigint(20) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `cedula` bigint(20) NOT NULL,
  `addinfo` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pacientes`
--

INSERT INTO `pacientes` (`id`, `nombre`, `apellido`, `cedula`, `addinfo`, `created_at`) VALUES
(2, 'Jesus Gabriel', 'Silva Marcano', 19143894, 'prueba', '2020-04-15 17:18:30'),
(3, 'Jesus Gabriel', 'Silva Marcano', 19143894, 'prueba', '2020-04-15 17:23:23'),
(4, 'q', 'q', 1, 'qq', '2020-04-15 17:30:22'),
(5, 'e', 'e', 22, 'ee', '2020-04-15 17:31:34'),
(6, '333', '333', 33, '333', '2020-04-15 17:32:58'),
(7, '5', '5', 5, '5', '2020-04-15 17:34:34'),
(8, '333', '333', 333, '333', '2020-04-15 17:35:12'),
(9, '444', '444', 444, '44', '2020-04-15 17:36:15'),
(10, '3333', '3333', 3333, '3333', '2020-04-15 17:36:41'),
(11, '222', '222', 22, '222', '2020-04-15 17:39:04'),
(12, '666', '6666', 666, '666', '2020-04-15 17:39:37'),
(13, '6666', '6666', 666, '666', '2020-04-15 17:39:46'),
(14, '3434343', '43434', 34343, '434343', '2020-04-15 17:39:52'),
(15, '343343', '434343', 43434, '343434', '2020-04-15 17:40:19'),
(16, '454545', '454545', 45454, '5454', '2020-04-15 17:42:54'),
(17, '5454545', '4545454', 5454545, '45454545', '2020-04-15 17:44:33'),
(18, '434343', '34343', 343434, '3434', '2020-04-15 17:46:14'),
(19, 'Arianna ', 'Cabrera', 20741757, 'Requiere Amoxicilina', '2020-04-15 18:34:27');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `pacientes`
--
ALTER TABLE `pacientes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `pacientes`
--
ALTER TABLE `pacientes`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
