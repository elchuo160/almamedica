function ObjetoAjax() {
	/* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo,
	por lo que se puede copiar tal como esta aqui */

	var xmlhttp=false;
	try{
		//creacion del objeto AJAX para navegadores no IE
		xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	}
	catch(e)
	{
		try{
			//creacion del objeto AJAX para IE
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(E)
		{
			if (!xmlhttp && typeof XMLHttpRequest!='undefined') xmlhttp=new XMLHttpRequest();
		}
	}
	return xmlhttp;
}




//validamos que todos los campos obtendan algo de informacion
function validar() {
	var nombreu        = $("#nombreu").val();
	var apellidou      = $("#apellidou").val();
	var cedulau        = $("#cedulau").val();
	var adicionalInfo  = $("#adicionalInfo").val();
	var photou         = $("#filename").val();
	var datos = [nombreu,apellidou,cedulau,adicionalInfo,photou];
	var msg = '';

	if (nombreu.length<=0)   { msg += 'Debe ingresar un Nombre \n'}
	if (apellidou.length<=0) { msg += 'Debe ingresar un Apellido \n'}
	if (cedulau.length<=0)   { msg += 'Debe ingresar un numero de Cedula \n'}
	if (photou.length<=0)    { msg += 'Debe cargar una imagen de Perfil \n'}

	if (msg.length>1) {
		alert(msg);
	}else{
		guardar(datos);
	}

}

function guardar(datos) {
	$.post('procesos/guardaru.php',{nombreu:datos[0],apellidou:datos[1],cedulau:datos[2],adicionalInfo:datos[3],photou:datos[4]},function(data){
   			$("#nombreu").val('');
			$("#apellidou").val('');
			$("#cedulau").val('');
			$("#adicionalInfo").val('');
			$("#uploadfile div.thumbnail").remove();
			$('#modal_alert').modal('show');
    });
}


function cargarLista() {
	$.post('../procesos/listau.php',{},function(data){
   			$("#listau").html(data);
   			$('#listau_tab').DataTable({
    			 "pagingType": "simple"
  			});
    });
}


$(function() {

    // preventing page from redirecting
    $("html").on("dragover", function(e) {
        e.preventDefault();
        e.stopPropagation();
        $("#title_photou").text("Foto");
    });

    $("html").on("drop", function(e) { e.preventDefault(); e.stopPropagation(); });

    // Drag enter
    $('.upload-area').on('dragenter', function (e) {
        e.stopPropagation();
        e.preventDefault();
        $("#title_photou").text("Suelta aqui");
    });

    // Drag over
    $('.upload-area').on('dragover', function (e) {
        e.stopPropagation();
        e.preventDefault();
        $("#title_photou").text("Suelta aqui");
    });

    // Drop
    $('.upload-area').on('drop', function (e) {
        e.stopPropagation();
        e.preventDefault();

        $("#title_photou").text("Upload");

        var file = e.originalEvent.dataTransfer.files;
        //console.log(file);
        var fd = new FormData();

        fd.append('file', file[0]);

        uploadData(fd);
    });

    // Open file selector on div click
    $("#uploadfile").click(function(){
        $("#file").click();
    });

    // file selected
    $("#file").change(function(){
        var fd = new FormData();

        var files = $('#file')[0].files[0];

        fd.append('file',files);

        uploadData(fd);
    });
});

// Sending AJAX request and upload file
function uploadData(formdata){

    $.ajax({
        url: 'procesos/upload.php',
        type: 'post',
        data: formdata,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(response){
            addThumbnail(response);
            //console.log(response);
        }
    });
}

// Added thumbnail
function addThumbnail(data){
    $("#uploadfile h1").remove(); 
    var len = $("#uploadfile div.thumbnail").length;

    var num = Number(len);
    num = num + 1;

    var name = data.name;
    var size = convertSize(data.size);
    var src = data.src.replace('../', '');

    $('#filename').val(name);
  
    console.log(src);
    // Creating an thumbnail
    $("#uploadfile").append('<div id="thumbnail_'+num+'" class="thumbnail"></div>');
    $("#thumbnail_"+num).append('<img src="'+src+'" width="100%" height="78%">');
    //$("#thumbnail_"+num).append('<span class="size">'+size+'<span>');

}

// Bytes conversion
function convertSize(size) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (size == 0) return '0 Byte';
    var i = parseInt(Math.floor(Math.log(size) / Math.log(1024)));
    return Math.round(size / Math.pow(1024, i), 2) + ' ' + sizes[i];
}