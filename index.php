<!DOCTYPE html>
<html>
<head>
	<!-- Jquery para habilitar funcionalidades de Boostrap -->
	<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<!-- estilo css global -->
	<link rel="stylesheet" type="text/css" href="css/global.css">

	<!-- script js para funciones asincronas-->
	<script type="text/javascript" src="js/ajax.js"></script>

	<title>AlmaMedica</title>
</head>
<body>
	<div class="container">
		<div id="logo"></div>		
		<ul class="nav nav-tabs">
			<li class="nav-item">
				<a class="nav-link active" href="index.php">Crear Perfil</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="vistas/lista.php">Lista de Perfiles</a>
			</li>
		</ul>
		
		<div class="card">
			<div class="card-body">
				<h1 class="card-title">AlmaMedica</h1>
				<h5 class="card-title">Formulario para registrar informacion basica de paciente:</h5>
				<form  method="post" enctype="multipart/form-data" >
					<div class="form-group">
						 <input type="file" name="file" id="file">
						 <input type="hidden" name="filename" id="filename">            
			            <!-- Drag and Drop container-->
			            <div class="upload-area"  id="uploadfile">
			                <br><h1 id="title_photou">Foto</h1>
			            </div>
			            <p align="center">
			            	<i>Puedes arrastrar y soltar la imagen o hacer click en el recuadro</i><br>
			            	<i>Solo pudes subir imagenes (jpg,png,bmp), por favor no subir imagenes superiores a 300kb</i>
			            </p>
					</div>
					<div class="form-group">
		    			<label for="nombreu">Nombre: </label>
		    			<input type="text" class="form-control" id="nombreu" name="nombreu" placeholder="Jesus Gabriel">
		  			</div>
		  			<div class="form-group">
		    			<label for="apellidou">Apellido: </label>
		    			<input type="text" class="form-control" id="apellidou" name="apellidou" placeholder="Silva Marcano">
		  			</div>
		  			<div class="form-group">
		    			<label for="cedulau">Cedula: </label>
		    			<input type="number" class="form-control" id="cedulau" name="cedulau" placeholder="19143894">
		  			</div>	
		  			<div class="form-group">
		    			<label for="adicionalInfo">Informacion adicional: </label>
		    			<textarea class="form-control" id="adicionalInfo" name="adicionalInfo" rows="3"></textarea>
		  			</div>
		  			<div class="form-group">
		  				<button type="button" class="btn btn-info" onclick="validar();"><span class="glyphicon glyphicon-floppy-save"></span> Guardar</button>
		  				
		  					<div class="modal" tabindex="-1" role="dialog" id="modal_alert">
							  <div class="modal-dialog" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <h5 class="modal-title">AlmaMedica</h5>
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							          <span aria-hidden="true">&times;</span>
							        </button>
							      </div>
							      <div class="modal-body">
							        <p>Los datos fueron almacenados con exito.</p>
							      </div>
							      <div class="modal-footer">
							        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
							        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							      </div>
							    </div>
							  </div>
							</div>
		  				
		  			</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>